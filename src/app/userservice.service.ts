import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  host: string="https://javagandhi.herokuapp.com/rest/";
  question: any[];
  options: any[];
  answer: string;
  questionId:string;
  qnprogress:number;

  constructor(private http: HttpClient) { 

  }


  doLogin(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    return this.http.get(this.host+"topic/getTopics",httpOptions)

  }
  domarks(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
    return this.http.get(this.host+"mark/getMarks",httpOptions)
  }
 
 getqQuestion(topic: string,mark: string){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
  }

  console.info(this.host+"question/getQuestions/"+topic.toLowerCase()+"/"+mark);
  return this.http.get(this.host+"question/getQuestions/"+topic.toLowerCase()+"/"+mark,httpOptions)
}



getResults(topic: string,mark: string,answers:any){
  let httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  }
   let body={"topic" :topic,"mark":mark,"answers" :answers}
   console.log("body");
   console.log(body);

   console.info("post url "+this.host+"result/getResult");
  return this.http.post(this.host+"result/getResult",body,httpOptions)
}











}
