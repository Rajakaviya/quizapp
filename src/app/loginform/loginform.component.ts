import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {

  loginForm: FormGroup;
  validateError="";
  constructor(private route: Router) {
   
   }

  ngOnInit() {

    this.loginForm = new FormGroup ({
      'name': new FormControl('',[Validators.required]),
      'password': new FormControl('',[Validators.required])
    });
  }

  get name(){return this.loginForm.get("name");}
  get password(){return this.loginForm.get("password");}



  onSubmit() {

    //alert(this.name.value+"........."+this.password.value);

    if(this.name.value=='admin' && this.password.value=='admin')
    {
    this.route.navigate(['topic']);
    }
    else{
      this.validateError="Please provide correct credentials";
    }



  }
  cancel()
    {
      
    }


  

  }
