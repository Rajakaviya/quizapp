import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginformComponent } from './loginform/loginform.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { Login } from './login';
import { LogicComponent } from './logic/logic.component';
import { TopicComponent } from './topic/topic.component';
import { UserserviceService } from './userservice.service';
import { QuestionService } from './question.service';
import {MatButtonModule,MatRadioModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { QuestionComponent } from './question/question.component';


const appRoutes: Routes = [
 { path: '',component:LoginformComponent },
 { path: 'main',component: MainComponent },
 { path: 'topic',component: TopicComponent },
 { path: 'logic',component: LogicComponent },
 { path: 'question',component: QuestionComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginformComponent,
    MainComponent,
    LogicComponent,
    TopicComponent,
    QuestionComponent,
   
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatRadioModule
  ],
  providers: [UserserviceService,QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule  {
  



 }
