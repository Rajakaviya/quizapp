import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserserviceService } from '../userservice.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { QuestionService } from '../question.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {

  marks: any;
  constructor( private route: Router,private userService: UserserviceService,private questionService: QuestionService) { }

  topics: any;
  topicForm: FormGroup;

  ngOnInit() {

    this.topicForm = new FormGroup ({
      'topic': new FormControl('',[Validators.required]),
      'mark': new FormControl('',[Validators.required])
    });

    this.userService.doLogin().subscribe(data=>{
      this.topics=data['data'];
    })
    this.userService.domarks().subscribe(data=>{
      this.marks=data['data'];
    })

  }

  get topic(){return this.topicForm.get('topic')};
  get mark(){return this.topicForm.get('mark')};

  onSubmit(){
    this.userService.getqQuestion(this.topic.value,this.mark.value).subscribe(data=>{
      this.questionService.questions=data['data'];
      this.questionService.selectedMark=this.mark.value;
      this.questionService.selectedTopic=this.topic.value;
      this.route.navigate(['logic']);

    })

   
 
  }

}
